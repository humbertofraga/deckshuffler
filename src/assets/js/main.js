---
---

var packs =
[
{% for pack in site.data.packs %}
  {
    "{{ pack[0] }}":
    [
    {% for card in pack[1] %}
    "{{ card }}"{% if forloop.last %}{% else %},{% endif %}
    {% endfor %}
    ]
  },
{% endfor %}
];

var draft_packs = [];

function shuffle(array) {
  for(var i = array.length; i > 1; i--) {
    var r = Math.floor(Math.random() * i);
    var temp = array[r];
    array[r] = array[i-1];
    array[i-1] = temp;
  }
}

function generate_deck() {
  shuffle(packs);
  var pack1 = packs[0];
  var pack2 = packs[1];
  $("#deckframe").show();
  $("#draft_accordion").hide();
  show_deck(pack1, pack2);
}

function show_deck(pack1, pack2) {
  $("#decklist").empty();
  var name1 = Object.keys(pack1)[0];
  var name2 = Object.keys(pack2)[0];
  $("#deckname").html(
    name1.replace(/_/g, " ").replace(/[0-9]$/, "")
    + " " +
    name2.replace(/_/g, " ").replace(/[0-9]$/, "")
  );
  for (var card of pack1[name1]) {
    var line = $("<span>")
      .addClass("cardname")
      .text(card + "\n");
    $("#decklist").append(line);
    $("#decklist").append($("<br>"));
  }
  for (var card of pack2[name2]) {
    var line = $("<span>")
      .addClass("cardname")
      .text(card + "\n");
    $("#decklist").append(line);
    $("#decklist").append($("<br>"));
  }
  $(".cardname").mouseenter(function(){
    getScryfallImage($(this).text().replace(/^[0-9]/, ""));
  });
}

function start_draft() {
  draft_packs = [];
  $("#deckname").html("Drafting packs");
  $("#deckframe").hide();
  $("#draft_accordion").removeClass("hidden");
  $("#draft_accordion").show();
  $("#draft_choice1").empty();
  $("#draft_choice2").empty();
  draw_draft_packs($("#draft_choice1"), on_pack1_selected);
  $("#collapse1").collapse('show');
}

function draw_draft_packs(container, callback) {
  shuffle(packs);
  for (const i of [0,1,2]) {
    var pack = packs[i];
    var name = Object.keys(pack)[0];
    var button = $("<button>")
      .addClass("btn btn-outline-dark mx-1")
      .text(name.replace(/_/g, " ").replace(/[0-9]$/, ""))
      .click([pack][0], callback)
      .appendTo(container);
  }
}

function on_pack1_selected(e) {
  var pack_1 = e.data;
  draft_packs.push(pack_1);
  var name = Object.keys(pack_1)[0];
  name = name.replace(/_/g, " ").replace(/[0-9]$/, "");
  $("#header1 > button").text("Pack #1: "+name);
  draw_draft_packs($("#draft_choice2"), on_pack2_selected);
  $("#collapse2").collapse('show');
}

function on_pack2_selected(e) {
  var pack_2 = e.data;
  draft_packs.push(pack_2);
  $("#draft_accordion").hide();
  $("#deckframe").show();
  show_deck(draft_packs[0], draft_packs[1]);
}

function getScryfallImage(cardname) {
  var cardname_safe = cardname.replace(/ /g, "+");
  $.ajax({
    url: "https://api.scryfall.com/cards/named?exact="+cardname_safe+"&format=json"
  }).done(function(data){
    $("#card_preview").attr("src", data.image_uris.normal);
  });
}

function copy_deck() {
  navigator.clipboard.writeText($("#decklist").text()).then(function() {
    show_toast("Copied to clipboard!");
  }, function() {
    show_toast("Error copying");
  });
}

function show_toast(message) {
  $("#toast_message").text(message);
  $("#toast").toast("show");
}

$(window).on("load", function() {
  generate_deck();
});
