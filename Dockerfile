FROM ruby:2.7

WORKDIR /usr/src/app

COPY Gemfile ./

RUN gem install bundler && \
    bundle install

COPY src ./
COPY Gemfile ./

RUN bundle exec jekyll build

COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh

ENTRYPOINT ["entrypoint.sh"]
EXPOSE 4000
