#!/usr/bin/env bash

# Convert deck files to a very basic YAML format
cd /usr/src/app
mkdir -p _data/packs
find data -type f -print0 | while read -d $'\0' file; do
  NAME=$(basename "${file%%.*}")
  echo Processing "$NAME"
  sed 's/^/- /' "$file" > "_data/packs/$NAME.yml"
done

bundle exec jekyll serve -H 0.0.0.0
